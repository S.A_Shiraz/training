import axios from 'axios';
import { put, takeEvery, takeLatest, takeLeading } from "redux-saga/effects";
import { getTodo, getTodoFail, getTodoSuccess } from "../reducer/todoReducer";

function* GetTodos({ payload }) {
    try {
        console.log("GET TODO SAGA", payload)

        const response = yield axios.get(`https://jsonplaceholder.typicode.com/todos/${payload}`);
        console.log(`Response ${payload}`, response);

        fetch("https://jsonplaceholder.typicode.com/todos", (response) => console.log('Response '));

        yield put(getTodoSuccess(response.data));
    } catch (e) {
        console.log("CATCH", e);
        yield put(getTodoFail(e));
    }
}

function* TodoSaga() {
    yield takeLeading(getTodo.type, GetTodos);
}

export default TodoSaga;