import { all } from "redux-saga/effects";
import TodoSaga from "./todoSaga";

export default function* rootSaga() {
    yield all([TodoSaga()]);
}
