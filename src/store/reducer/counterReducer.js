import { createSlice } from '@reduxjs/toolkit';

export const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        value: 0,
    },
    reducers: {
        increase: (state, payload) => {
            state.value += 1;
        },
        decrease: state => {
            state.value -= 1
        },
        reset: state => {
            state.value = 0
        }
    }
})

export const {
    increase,
    decrease,
    reset
} = counterSlice.actions;
export default counterSlice.reducer;