import { createSlice } from '@reduxjs/toolkit';

export const todoSlice = createSlice({
    name: 'todo',
    initialState: {
        todos: [],
        loading: false,
        error: undefined
    },
    reducers: {
        getTodo: state => {
            state.loading = true;
        },
        getTodoSuccess: (state, { payload }) => {
            state.loading = false;
            state.todos = payload;
        },
        getTodoFail: (state, payload) => {
            state.loading = false;
            state.todos = [];
            state.error = payload;
        }
    }
})

export const {
    getTodo,
    getTodoSuccess,
    getTodoFail
} = todoSlice.actions;
export default todoSlice.reducer;