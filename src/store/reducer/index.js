import { combineReducers } from "@reduxjs/toolkit";
import CounterReducer from "./counterReducer";
import TodoReducer from "./todoReducer";

const allReducers = combineReducers({
    counter: CounterReducer,
    todo: TodoReducer
});

export default allReducers;
