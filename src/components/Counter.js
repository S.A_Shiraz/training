import React from "react";
import { useSelector, useDispatch } from 'react-redux';
import { decrease, increase } from '../../src/store/reducer/counterReducer';
import './styles.css';

function Counter() {
    const count = useSelector(state => state.counter?.value)
    const dispatch = useDispatch()

    return (
        <div className="counter">
            <button onClick={() => dispatch(increase())}>
                Increase
            </button>
            <p>{count}</p>
            <button onClick={() => dispatch(decrease())}>
                Decrease
            </button>
        </div>
    );
}

export default Counter;