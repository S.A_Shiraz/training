const useMultiplier = (num, multiplier) => {
    return num * multiplier
};

export default useMultiplier;