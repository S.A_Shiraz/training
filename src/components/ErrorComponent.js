import React from "react";

export default class ErrorComponent extends React.Component {
    // GFGComponent throws error as state of
    // GFGCompnonent is not defined

    render() {
        return <h1>{this.state.heading}</h1>;
    }
}