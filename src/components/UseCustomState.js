const useMultiplierHook = (num, multiplier) => {
    let result = num * multiplier;
    return [result, () => console.log(`The result is ${result}`)];
};

export default useMultiplierHook;