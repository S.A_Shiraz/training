import React, { useCallback, useState } from "react";
import ChildComponent from "./ChildComponent";

const UseCallBackComponent = () => {
    const [count, setCount] = useState(1000);

    const increment = useCallback(() => {
        setCount((c) => c + 1);
    }, []);

    const testFunction = useCallback(() => {
        console.log("count", count);
    }, [count]);

    // const testFunction = () => {
    //     console.log("count", count);
    // };

    return (
        <>
            <div
                style={{
                    width: 400,
                    height: 600,
                    backgroundColor: 'blue',
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                Count: {count}
                <button onClick={increment}>Increment</button>


                <div style={{ width: 300, height: 400, backgroundColor: 'green' }}>
                    <ChildComponent testFunction={testFunction} />
                </div>
            </div>
        </>
    );
};

export default UseCallBackComponent;