import React from "react";
import ErrorComponent from "./ErrorComponent";

export default class ClassComponent extends React.Component {
    constructor(props) {
        super();
        console.clear();
        console.log("constructor", props)
        this.state = {
            num: 0,
            error: undefined,
            name: ''
        };
    }

    componentDidMount() {
        console.log("componentDidMount");
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log("PREVIOS STATE", this.state);
        // console.log("shouldComponentUpdate", { nextState });

        if (nextState.num % 2 == 0) {
            return true
        } else {
            return false
        }
    }

    render() {
        return (
            <>
                {
                    this.state.error ?
                        <div>THERE IS AN ERROR</div>
                        :
                        <>
                            <ErrorComponent />
                            <h1>My Favorite Number is {this.state.num}</h1>
                            <button onClick={() => this.setState({ num: 2 })}>Update Class</button>
                        </>
                }
            </>
        );
    }

    componentDidUpdate(prevProps, prevState) {
        console.log("componentDidUpdate", { prevProps, prevState });
    }

    componentWillUnmount() {
        console.log("componentWillUnmount");
    }

    componentDidCatch(error, info) {
        console.log("componentDidCatch", { error, info });
        this.setState({ error });
    }
}