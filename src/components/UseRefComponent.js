import React, { useEffect, useRef, useState } from "react";

function UseRefComponent() {
    // // let value = 0;
    // const value = useRef(0); // {current: 0}
    // const [state, setState] = useState(true);

    // useEffect(() => {
    //     console.log("STATE UPDATED", state);
    //     console.log("Value is still", value);
    // }, [state]);

    // return (
    //     <>
    //         <div>Value is {value.current}</div>
    //         {/* <button
    //             onClick={() => {
    //                 value = value + 1;
    //             }}>
    //             Update Value
    //         </button> */}

    //         <button
    //             onClick={() => {
    //                 value.current = value.current + 1;
    //             }}>
    //             Update Ref Value
    //         </button>

    //         <button
    //             onClick={() => setState(!state)}
    //         >
    //             Update State
    //         </button>
    //     </>
    // );
    const elem = useRef();

    const focusInput = () => {
        elem.current.focus();
    };

    return (
        <>
            <input type="text" ref={elem} />
            <button onClick={focusInput}>Focus Input</button>
        </>
    );
}

export default UseRefComponent;