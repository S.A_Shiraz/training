import React, { memo } from "react";

const ChildComponent = ({ testFunction }) => {
    return (
        <>
            {console.log("child render")}
            <button onClick={testFunction}>Run Test Function</button>
        </>
    );
};

export default memo(ChildComponent);