import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
} from "react-router-dom";
import Home from '../screens/home';
import About from '../screens/about';
import Users from '../screens/users';
import NestedHome from "../screens/nestedHome";

export default function MainRouter() {
    return (
        <Router>
            <div>
                <div style={{ display: 'flex', justifyContent: 'space-around', margin: 50 }}>
                    <div>
                        <Link to="/">Home</Link>
                    </div>

                    <div>
                        <Link to="/about">About</Link>
                    </div>

                    <div>
                        <Link to="/users">Users</Link>
                    </div>

                    <div>
                        <Link to="/nono">No Page</Link>
                    </div>

                    <div>
                        <Link to="/nested">Nested Page</Link>
                    </div>
                </div>

                <div style={{ marginBottom: 50 }}>-----------------Parent END-----------------</div>

                {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
                <Switch>
                    <Route path="/about">
                        <About />
                    </Route>
                    <Route path="/users">
                        <Users />
                    </Route>
                    <Route path="/nested">
                        <NestedHome />
                    </Route>
                    <Route path="/">
                        <Home />
                    </Route>
                    <Route>
                        <Redirect to='/' />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}
