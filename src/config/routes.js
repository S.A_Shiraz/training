import About from "../screens/about";
import Home from "../screens/home";
import Users from "../screens/users";

const Routes = [
    {
        url: '/about',
        component: About,
        isPublic: true
    },
    {
        url: '/users',
        component: Users,
        isPrivate: true
    },
    {
        url: '/',
        component: Home,
        isPrivate: true
    }
];

export default Routes;