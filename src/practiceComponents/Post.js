import React, { useEffect, useState } from 'react';

function Post({
    post,
    setNum,
    num
}) {
    useEffect(() => {
        console.log("PROPS IN CHILD", post);
    }, []);

    return (
        <>
            <div className="App">
                I am a post with props {post}

                <div>
                    <button onClick={() => setNum(num + 1)}>Update Parent</button>
                </div>
            </div>
        </>
    );
}

export default Post;
