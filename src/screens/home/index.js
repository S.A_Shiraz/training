import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Counter from "../../components/Counter";
import { getTodo } from '../../store/reducer/todoReducer';
import axios from 'axios';

// https://jsonplaceholder.typicode.com/todos

export default function Home() {
    const dispatch = useDispatch();
    const count = useSelector(state => state.counter?.value)
    const loading = useSelector(state => state.todo?.loading)
    const todos = useSelector(state => state.todo?.todos)

    console.log("loading", loading);

    useEffect(() => {
        for (let i = 1; i <= 10; i++) {
            dispatch(getTodo(i));
        }
    }, []);

    useEffect(() => {
        console.log("TODOS IN COMPONENT", todos);
    }, [todos]);

    if (loading) {
        return <h4>Loading...</h4>
    }

    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'space-around',
                alignItems: 'center'
            }}>
            <div>
                <Counter />
            </div>

            <div
                style={{
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                Count is: {count}
            </div>

            {/* {
                loading ?
                    <div>
                        Loadnig
                    </div>
                    : null
            } */}
        </div>
    );
}