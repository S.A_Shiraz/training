import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
} from "react-router-dom";

export default function NestedHome() {
    return (
        <Router>
            <div>
                <div style={{ display: 'flex', justifyContent: 'space-around', margin: 20, backgroundColor: '' }}>
                    <div>
                        <Link to="/home/page-one">Page 1</Link>
                    </div>

                    <div>
                        <Link to="/home/page-two">Page 2</Link>
                    </div>

                    <div>
                        <Link to="/home/page-three">Page 3</Link>
                    </div>
                </div>


                {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
                <Switch>
                    <Route path="/home/page-one">
                        <Page1 />
                    </Route>
                    <Route path="/home/page-two">
                        <Page2 />
                    </Route>
                    <Route path="/home/page-three">
                        <Page3 />
                    </Route>
                    <Route>
                        <Redirect to='/home/page-one' />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

function Page1() {
    return <h2>Page 1</h2>;
}

function Page2() {
    return <h2>Page 2</h2>;
}

function Page3() {
    return <h2>Page 3</h2>;
}